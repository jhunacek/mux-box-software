#!/usr/bin/env python3

import sys
import argparse

try:
	import serial.abc # Sub-module in serial but not pyserial
	sys.exit("It appears you have serial installed, which conflicts with the required package pyserial.  Please remove serial and install pyserial.")
except ImportError:
	# The offending package isn't present, good
	pass

try:
	import serial
except ImportError:
	sys.exit("Please install pyserial")

# Command line arguments
parser = argparse.ArgumentParser(
	formatter_class=argparse.RawDescriptionHelpFormatter,
	description='Multiplexer box control script.', 
	epilog='Simple example:\n   ./setmux.py -a 12 -b 128\n ')
parser.add_argument('-a','--pina', type=int, nargs='?', default=None, help='Pin number (i.e., mux address+1) to set for mux tree A')
parser.add_argument('-b','--pinb', type=int, nargs='?', default=None, help='Pin number (i.e., mux address+1) to set for mux tree B')
parser.add_argument('-c','--comport', type=str, nargs='?', default='/dev/ttyACM0', help='The serial port the device is connected to (defaults to /dev/ttyACM0, should be of the form COM1 on Windows)')

# They really should pass at least one argument.  If not, show the help.
if len(sys.argv) <= 1: 
	parser.print_help()
	exit()

args = parser.parse_args()

s = serial.Serial(args.comport, 115200)

# Set the two mux trees
for tree_id, value in [('a',args.pina),('b',args.pinb)]:
	if (value is not None):
		if (value >= 1) and (value <= 256):
			s.write(b"{%b,%i}" % (tree_id.encode(), value))
			print("Setting mux tree %s to pin %i" % (tree_id, value))
		else:
			print("Invalid pin number for mux tree %s" % tree_id)
		
