

// Pin IDs for the mux channels, from LSB to MSB
const uint8_t NUM_TREES = 2;
const uint8_t NUM_BITS = 8;
const int16_t PINS_MUX[NUM_TREES][NUM_BITS] = {{8,9,10,11,12,A0,A1,A2},{0,1,2,3,4,5,6,7}}; 

// Packet settings
#define MIN_PACKET 3
#define MAX_PACKET 32
const char PACKET_START = '{';
const char PACKET_DELIM = ',';
const char PACKET_DELIMS[] = ",";
const char PACKET_END = '}';

// Set mux tree [0,1] to a given mux_address [0,255].
// Note mux_address is 0 indexed, pins are 1 indexed.
void set_mux(uint8_t tree, uint8_t mux_address)
{
  for (int b = 0; b < NUM_BITS; b++)
  {
    digitalWrite(PINS_MUX[tree][b], !((mux_address >> b) & 0x01));
  }

  // Inform the user
  Serial.print(PACKET_START);
  Serial.print("set");
  Serial.print(PACKET_DELIM);
  Serial.print((char)(tree + 'a'));
  Serial.print(PACKET_DELIM);
  Serial.print(mux_address + 1); // Send pin, not address
  Serial.println(PACKET_END);
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
}

void setup() 
{
  Serial.begin(115200);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  
  for (int t = 0; t < NUM_TREES; t++)
  {
    for (int b = 0; b < NUM_BITS; b++)
    {
      pinMode(PINS_MUX[t][b], OUTPUT);
      digitalWrite(PINS_MUX[t][b], HIGH);
    }   
  }
  
  Serial.print(PACKET_START);
  Serial.print("boot");
  Serial.println(PACKET_END);

  // Default to open circuit
  set_mux(0, 255);
  set_mux(1, 255);
}

void loop() 
{
  char packet[MAX_PACKET];
  int packet_len = 0;

  // Wait for a packet start character
  while(Serial.read() != PACKET_START) {}

  // Read the packet up to the end delimiter
  packet_len = Serial.readBytesUntil(PACKET_END, packet, MAX_PACKET-1);
  packet[packet_len] = 0; // Null terminate for strtok

  if (packet_len < MIN_PACKET)
    return;

  // Read in the desired mux tree
  char* tree_str = strtok(packet, PACKET_DELIMS);
  int tree = tree_str[0] - 'a'; // 0 for a, 1 for b  

  // Read in the desired address
  int pin = atoi(strtok(NULL, PACKET_DELIMS));
  int address = pin - 1;

  if ((tree >= 0) && (tree <= 1) && (address >=0) && (address <= 255))
  {
    set_mux(tree, address);    
  }
  else
  {
    Serial.print(PACKET_START);
    Serial.print("error");
    Serial.println(PACKET_END);
  }
}
